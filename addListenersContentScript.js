function execute() {
    if (document.querySelector('#report-btn')) {
        document.querySelector('#report-btn').addEventListener('click', () => {
            var diraskSearchPopup = document.createElement('div');
            diraskSearchPopup.id = 'dirask-search-popup';
            diraskSearchPopup.innerHTML = `
                <div id='dirask-search-popup-inner'>
                    <form id="report-form", onsubmit="document.querySelector('#dirask-search-popup').remove();"> 
                        <div id='dirask-search-popup-inner-container'>
                            <span>Describe your problem:</span>
                            <textarea
                                type='text'
                                style='height: 70px; resize: none'
                            ></textarea>
                            <button>Report</button>
                        </div>
                    </form>
                </div>`;
            document.querySelector('body').appendChild(diraskSearchPopup);
        });
    } else {
        setTimeout(execute, 30); // temporary solution 
    }
}

execute();
