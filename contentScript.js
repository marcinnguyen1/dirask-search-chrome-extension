chrome.runtime.onMessage.addListener((request) => {
    if (request.text) {
        updatePostList(request.text);
    }
});

var diraskSearchPostsList = document.createElement('div');
diraskSearchPostsList.id = 'dirask-search-list';

var container = document.querySelector('#rhs > div');
container.insertBefore(diraskSearchPostsList, container.firstChild);

var inputDiv = document.createElement('div');
inputDiv.innerHTML =
    '<input id="dirask-input" type="text" placeholder="Search..." />';
var tmpContainer = document.querySelector('#rhs > div');
tmpContainer.insertBefore(inputDiv, tmpContainer.firstChild);

var diraskInput = document.querySelector('#dirask-input');
diraskInput.addEventListener('input', (event) => {
    chrome.runtime.sendMessage({
        query: event.target.value,
    });
});

{
    var initialSearchQuery = document.querySelector(
        '#tsf > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input'
    ).value;
    document.querySelector('#dirask-input').value = initialSearchQuery;
    chrome.runtime.sendMessage({
        query: initialSearchQuery,
    });
}

function updatePostList(dataToInject) {
    diraskSearchPostsList.innerHTML = dataToInject;
}
