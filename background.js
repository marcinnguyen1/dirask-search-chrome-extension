function makeQuery(query) {
    var queryArray = query.trim().replace(/ +/g, ' ').split(' ');
    if (queryArray[0] === '') return;
    var queryString = 'https://dirask.com/suggestions/search-posts?query=';

    var queryLength = queryArray.length;
    if (queryLength === 1) {
        queryString += queryArray[0];
        queryString += '&criteria=%5B%5D&type=wiki';
    } else {
        queryString += queryArray[queryLength - 1];
        queryString += '&criteria=%5B%22';

        for (var i = 0; i < queryLength - 1; i++) {
            queryString += queryArray[i];
            if (i < queryLength - 2) {
                queryString += '%22%2C%22';
            }
        }
        queryString += '%22%5D&type=wiki';
    }
    return queryString;
}

function makeTable(data) {
    var a = `<div id="dirask-search"><ul>`;

    var maxLength = data.suggestions.length < 7 ? data.suggestions.length : 7;
    for (i = 0; i < maxLength; i++) {
        a += `<li>
                <a href="https://dirask.com/posts/${data.suggestions[i].post_hash}" >${data.suggestions[i].post_name}</a>
            </li>`;
    }
    a += `</ul>
            <div>
                <span>There is no solution to your problem here?</span>
                <span>
                    <a id="report-btn" href="javascript:;"
                        >Report a problem on the dirask</a>
                    - our community will try to help you!
                </span>
            </div>
        </div>`;
    return a;
}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if (request.query) {
        fetch(makeQuery(request.query))
            .then((response) => response.json())
            .then((data) => fetchSuggestion(makeTable(data)));

        const fetchSuggestion = (data) => {
            chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
                chrome.tabs.sendMessage(tabs[0].id, { text: data });
            });
        };
    }
});
